package com.orbitall.sistemacapuano;

import com.orbitall.sistemasolar.Marte;
import com.orbitall.sistemasolar.Planeta;
import com.orbitall.sistemasolar.Terra;

public class Main {
	public static void main(String[] args) {
		Planeta terra = new Terra();
		Planeta marte = new Marte();
		
		System.out.println(terra);
		System.out.println(marte);
		
	}
}