/**
 * 
 */
package com.orbitall.sistemasolar;

/**
 * @author asbastos
 *
 */
public class Marte extends Planeta {

	public Marte() {
		super();
		habitavel = true;
		nome = "Marte";
		raio = "3.390 km";
		gravidade = "3,711 m/s�";
	}
	
}
