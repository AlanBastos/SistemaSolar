package com.orbitall.sistemasolar;

/**
 * � Copyright Bastos Corp 2017 <br/><br/>
 *
 * O objetivo da classe <code>com.orbitall.sistemasolar.Terra</code> � trazer
 * os mais importantes detalhes do planeta <code>com.orbitall.sistemasolar.Terra</code>.
 * Vamos filosofar.... o que � <code>com.orbitall.sistemasolar.Terra</code>? A Terra � o
 * terceiro planeta mais pr�ximo do Sol, o mais denso e o quinto maior dos oito planetas
 * do Sistema Solar. � tamb�m o maior dos quatro planetas tel�ricos. � por vezes designada
 * como Mundo ou Planeta Azul.
 * 
 * @since JDK1.8
 * @version 1.0
 * @author asbastos@stefanini.com
 * @see com.orbitall.sistemasolar.Planeta
 */
public class Terra extends Planeta {
	/**
	 * Construtor padr�o do planeta Terra.
	 */
	public Terra() {
		super();
		this.habitavel = true;
		this.nome = "Terra";
		this.raio = "6.371 km";
		this.gravidade = "9,807 m/s�";
	}
}