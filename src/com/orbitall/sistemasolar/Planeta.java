package com.orbitall.sistemasolar;

/**
 * � Copyright Bastos Corp 2017 <br/><br/>
 *
 * A classe abstrata <code>com.orbitall.sistemasolar.Planeta</code> ser� a classe
 * para todos os planetas que est�o envolvidos no sistema solar.
 * 
 * @since JDK1.8
 * @version 1.0
 * @author asbastos@stefanini.com
 * @see java.lang.Object
 */
public abstract class Planeta extends Object {
	/**
	 * O atributo habit�vel � referente ao planeta se ele � habit�vel ou n�o.
	 */
	protected boolean habitavel;
    
	/**
	 * O atributo nome � referente ao nome do planeta.
	 */
	protected String nome;

    /**
     * O atributo raio � referente ao raio do planeta, vamos filosofar! O raio 
     * terrestre � a dist�ncia entre o centro da Terra e sua superf�cie. Devido 
     * ao fato de que a Terra n�o � uma esfera perfeita, n�o h� um �nico valor 
     * que sirva como seu raio natural. Ao inv�s disso, sendo quase esf�rica, 
     * v�rios valores, desde o raio polar (de 6 357 km) ao raio equatorial (6 378 km) 
     * s�o utilizados, de acordo com a necessidade, e modelos da Terra assumindo esta 
     * como uma esfera geram um raio m�dio de 6 371 km.
     */
	protected String raio;
	
	/**
	 * A propriedade gravidade � referente a gravidade do planeta, a pergunta: -O que
	 * � a gravidade? Trata-se de uma das quatro for�as fundamentais da natureza e est� 
	 * relacionada com os efeitos da atra��o m�tua existente entre corpos massivos.
	 */
	protected String gravidade;
	
	/**
	 * Construtor padr�o da classe <code>com.orbitall.sistemasolar.Planeta</code>.
	 */
	public Planeta() {
		super();
	}
	
	/**
	 * @return the habitavel
	 */
	public boolean isHabitavel() {
		return habitavel;
	}

	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * @return the raio
	 */
	public String getRaio() {
		return raio;
	}

	/**
	 * @return the gravidade
	 */
	public String getGravidade() {
		return gravidade;
	}

	/**
	 * O m�todo toString() imprimir� todos os atributos do Planeta.
	 * @return String - todos os atributos do Planeta.
	 */
	@Override
	public String toString() {
		return "{" + getClass().getName() + 
				", habitavel:[" + habitavel + 
				"], nome:[" + nome + 
				"], raio:[" + raio + 
				"], gravidade:[" + gravidade + 
				"]}";
	}
}